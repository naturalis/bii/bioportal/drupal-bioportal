# Bioportal

This project constists of the drupal installation and configuration of Bioportal.
This is web application that can search in the NDBS (formerly also known as NBA)
and present results in the browser.
The public can browse through the Dutch Nationaly History collections.

## Development

Although the content and functionality of the site can be managed in
production/acceptance, it is important to keep the development of
this site in sync. Any real configuration changes and major content
changes should be handled by updating this project and you need
to do this by running this site locally.

### Required software

 1. PHP (preferably same major.minor version as the php-fpm docker container, see docker/php-fpm/Dockerfile)
 2. Composer ([latest](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos))
 3. docker ([latest](https://docs.docker.com/engine/install/))
 4. docker compose ([latest](https://docs.docker.com/compose/install/))

### Development set up

Start by cloning this project,
choose the correct branch (normally 'develop', 'master'/'main' or the feature branch you need to check).

Tota

 1. Run `composer install` in the project directory:

```
 docker run --rm --interactive --tty \
  --volume $PWD:/var/www \
  registry.gitlab.com/naturalis/bii/drupal/docker-drupal/php-dev:latest /bin/sh -c 'composer install'
```

 2. Run `docker login registry.gitlab.com` ([see GitLab documentation](https://gitlab.com/help/user/packages/container_registry/index#authenticating-to-the-gitlab-container-registry))
 3. `mkdir -p data/database/init` to create the init directory
 4. Copy a recent database dump in this init directory
 5. Create an .env file containing `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` of the **dryrun.link** domain (see bitwarden)
 5. Run `docker compose up` in a different cli tab (to see the log output right away)
 6. Add `127.0.0.1 bioportal.dryrun.link` to your hosts file (/etc/hosts)
 7. Open [bioportal.dryrun.link](https://bioportal.dryrun.link) in your favorite browser

If you do not see a site. You should also run `drush cc all`:

```
docker compose exec php-fpm sh -c './vendor/bin/drush cr'
```

If things are still not showing, look at the logs (`docker compose logs -f`) for more clues.

### Upgrading

Every once in a while drupal does a system or module upgrade. You need to upgrade and to create a new version of this project.

 1. Fetch and check out the latest develop branch
 2. Create a new branch `git checkout -b upgrade-dependencies`
 3. Run `composer update`
 4. See with `git status` if there are any changes to composer.json and/or composer.lock
 5. Commit and push these changes, create a merge request
 6. See if the pipeline builds correctly
 7. To be sure, run the site locally or deploy to acceptance by running the pipeline with the current branch name
 8. If everything is okay, merge to develop
 9. Update the develop branch locally, create a new tag
 10. Push the tag
 11. Deploy the new tag after is build

## Trivyignore

The gitlab pipeline checks the security of the build docker images using
[trivy](https://trivy.dev/).
Whenever trivy throws up security issues that stop the pipeline but
can't be fixed yet you can add a CVE number of the issue to the
ignore file.
This process has been centralized in
[lib / trivyignore](https://gitlab.com/naturalis/lib/trivyignore),
you should follow the
[instructions](https://gitlab.com/naturalis/lib/trivyignore#procedure) in the project.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`

