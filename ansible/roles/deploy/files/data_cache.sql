CREATE TABLE IF NOT EXISTS `data_cache` (
  `project` varchar(32) NOT NULL,
  `field` varchar(255) NOT NULL,
  `data` longblob DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`project`,`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
