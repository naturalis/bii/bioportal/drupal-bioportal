server {
    listen 80 default_server;

    server_name  _;

    access_log /proc/self/fd/1 json_combined;
    error_log /proc/self/fd/2 warn;

    root /var/www/public;
    index index.php;

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location = /favicon.ico  {
        expires max;
        add_header Cache-Control "public, immutable";
        log_not_found off;
        access_log off;
    }

    location = /sitemap-index.xml {
        root /var/www/public/sites/default/files/sitemap;
        allow all;
    }

    location ~ ^/sitemap/ {
        root /var/www/public/sites/default/files;
        allow all;
    }

    location ~ ^/sites/.*/files/styles/ {
        try_files $uri @rewrite;
        expires max;
        add_header Cache-Control "public, immutable";
        log_not_found off;
    }

    location ~* \.(js|css|png|jpe?g|gif|ico|svg|woff2?|ttf|eot|docx?|pdf|json|wav|mp3|mp4|ogg|webm|txt|gz|yml)$ {
        expires max;
        add_header Cache-Control "public, immutable";
        log_not_found off;
    }

    # Don't allow direct access to files in the core directory.
    location ~ /core/ {
        deny all;
        return 404;
    }

    # Don't allow direct access to PHP files in the vendor directory.
    location ~ /vendor/.*\.php$ {
        deny all;
        return 404;
    }
    # Deny access to dotfiles
    location ~ (^|/)\. {
        return 403;
    }
    # Deny access to web.config
    location ~ (^|/)web.config {
        return 403;
    }
    # Deny access to common Drupal files
    location ~ ^/(core/install.php|core/rebuild.php|update.php)$ {
         return 403;
    }
    # Allow "Well-Known URIs" as per RFC 5785
    location ~* ^/.well-known/ {
        expires max;
        add_header Cache-Control "public, immutable";
        allow all;
    }
    # Fallback
    location / {
        try_files $uri $uri/ @rewrite;
    }

    location ~ \.php$ {
        add_header 'Access-Control-Allow-Origin' '*' always;
        fastcgi_split_path_info ^(.+?\.php)(|/.*)$;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param QUERY_STRING $query_string;
        fastcgi_intercept_errors on;
        fastcgi_read_timeout 60;
        include fastcgi_params;
        fastcgi_pass php-fpm:9000;
    }

    location @rewrite {
        rewrite ^ /index.php;
    }


    client_max_body_size 12M;

    proxy_buffers 16 16k;
    proxy_buffer_size 16k;

    gzip on;
    gzip_proxied any;
    gzip_static on;
    gzip_http_version 1.0;
    gzip_vary on;
    gzip_comp_level 6;
    gzip_types
        application/font-woff
        application/font-woff2
        application/javascript
        application/json
        application/vnd.ms-fontobject
        application/x-font-opentype
        application/x-font-ttf
        application/x-javascript
        application/xhtml+xml
        application/xml
        application/xml+rss
        image/svg+xml
        image/x-icon
        text/css
        text/javascript
        text/plain
        text/xml;
    gzip_buffers 16 16k;
    gzip_min_length 512;
}
