/* This file should restore the auto increment of unique id's */
/* block_content */
ALTER TABLE `block_content` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`id`) FROM `block_content`;
SET @sql := CONCAT('ALTER TABLE `block_content` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* block_content_revision */
ALTER TABLE `block_content_revision` MODIFY `revision_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`revision_id`) FROM `block_content_revision`;
SET @sql := CONCAT('ALTER TABLE `block_content_revision` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* comment */
ALTER TABLE `comment` MODIFY `cid` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`cid`) FROM `comment`;
SET @sql := CONCAT('ALTER TABLE `comment` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* file_managed */
ALTER TABLE `file_managed` MODIFY `fid` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`fid`) FROM `file_managed`;
SET @sql := CONCAT('ALTER TABLE `file_managed` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* menu_link_content */
ALTER TABLE `menu_link_content` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`id`) FROM `menu_link_content`;
SET @sql := CONCAT('ALTER TABLE `menu_link_content` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* menu_link_content_revision */
ALTER TABLE `menu_link_content_revision` MODIFY `revision_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`revision_id`) FROM `menu_link_content_revision`;
SET @sql := CONCAT('ALTER TABLE `menu_link_content_revision` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* menu_tree */
ALTER TABLE `menu_tree` MODIFY `mlid` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`mlid`) FROM `menu_tree`;
SET @sql := CONCAT('ALTER TABLE `menu_tree` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* node */
ALTER TABLE `node` MODIFY `nid` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`nid`) FROM `node`;
SET @sql := CONCAT('ALTER TABLE `node` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* node_revision */
ALTER TABLE `node_revision` MODIFY `vid` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`vid`) FROM `node_revision`;
SET @sql := CONCAT('ALTER TABLE `node_revision` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* path_alias */
ALTER TABLE `path_alias` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`id`) FROM `path_alias`;
SET @sql := CONCAT('ALTER TABLE `path_alias` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* path_alias_revision */
ALTER TABLE `path_alias_revision` MODIFY `revision_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`revision_id`) FROM `path_alias_revision`;
SET @sql := CONCAT('ALTER TABLE `path_alias_revision` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* queue */
ALTER TABLE `queue` MODIFY `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`item_id`) FROM `queue`;
SET @sql := CONCAT('ALTER TABLE `queue` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* sequences */
ALTER TABLE `sequences` MODIFY `value` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`value`) FROM `sequences`;
SET @sql := CONCAT('ALTER TABLE `sequences` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* shortcut */
ALTER TABLE `shortcut` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`id`) FROM `shortcut`;
SET @sql := CONCAT('ALTER TABLE `shortcut` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* taxonomy_term_data */
ALTER TABLE `taxonomy_term_data` MODIFY `tid` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`tid`) FROM `taxonomy_term_data`;
SET @sql := CONCAT('ALTER TABLE `taxonomy_term_data` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* taxonomy_term_revision */
ALTER TABLE `taxonomy_term_revision` MODIFY `revision_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`revision_id`) FROM `taxonomy_term_revision`;
SET @sql := CONCAT('ALTER TABLE `taxonomy_term_revision` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* users */
UPDATE `users` SET `uid` = 9990 WHERE `uid` = 0;
UPDATE `users` SET `uid` = 9991 WHERE `uid` = 1;
ALTER TABLE `users` MODIFY `uid` int(10) unsigned NOT NULL AUTO_INCREMENT;
UPDATE `users` SET `uid` = 0 WHERE `uid` = 9990;
UPDATE `users` SET `uid` = 1 WHERE `uid` = 9991;
SELECT @max_id:=MAX(`uid`) FROM `users`;
SET @sql := CONCAT('ALTER TABLE `users` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;

/* watchdog */
ALTER TABLE `watchdog` MODIFY `wid` int(10) unsigned NOT NULL AUTO_INCREMENT;
SELECT @max_id:=MAX(`wid`) FROM `watchdog`;
SET @sql := CONCAT('ALTER TABLE `watchdog` AUTO_INCREMENT = ', IFNULL(@max_id, 0)+1);
PREPARE stmt FROM @sql;
EXECUTE stmt;
