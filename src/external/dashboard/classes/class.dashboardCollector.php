<?php
include_once("class.dataCache.php");
include_once("class.collectionUnitCalculation.php");

// 2019.03.22: voor actuele minus-waarde:
// https://api.biodiversitydata.nl/v2/specimen/count/?collectionType=Arts
define('ARTS_COUNT', 2449);
define('BRAHMS_LOWER_PLANTS_COUNT', 13527);
define('BRAHMS_LOWER_PLANTS_AVG', 40.30);
define('TWO_D_COUNT', 625500);
define('STONES_NBA_DISCARDED_COUNT', 199000);

class dashboardCollector
{
  private dataCache $cache;
  private $data;

  private collectionUnitCalculation $calculator;

  private $staticNumbers = [
      'BrahmsLowerPlants' => [
        'category' => 'Botanie lage planten',
        'storageNumber' => BRAHMS_LOWER_PLANTS_COUNT,
        'average' => BRAHMS_LOWER_PLANTS_AVG
      ],
      '2D' => [
        'category' => '2D materiaal',
        'specimenNumber' => (TWO_D_COUNT - ARTS_COUNT),
        'average' => 1
      ],
      'StonesNBADiscarded' => [
        'category' => 'Mineralogie en petrologie',
        'specimenNumber' => STONES_NBA_DISCARDED_COUNT,
        'average' => 1
      ],
    ];

  public function __construct()
  {
    $this->cache = new dataCache();
    $this->cache->setDbParams(config::databaseAccessParameters());
    $this->cache->setProject('nba_cache');

    $this->data = $this->cache->getData();
    $this->setup_calculator();
  }

  private function setup_calculator()
  {
    $this->calculator = new collectionUnitCalculation();

    $this->calculator->setStorageSumPerCollWithIndivCount( $this->data->storage_sumPerColl_withIndivCount );
    $this->calculator->setStorageSumPerCollWithoutIndivCount( $this->data->storage_docCountPerColl_withoutIndivCount );
    $this->calculator->setSpecimenPrepTypePerCollection( $this->data->specimen_prepTypePerCollection );
    $this->calculator->setSpecimenNoPrepTypePerCollection( $this->data->specimen_noPrepTypePerCollection );
    $this->calculator->setSpecimenKindOfUnitPerCollection( $this->data->specimen_kindOfUnitPerCollection );

    foreach((array)$this->staticNumbers as $staticNumber)
    {
      /*
          please also note this line in the collectionUnitCalculation class:
          if ( $category == 'entomologie' ) continue;
          there is overlap between specimens and storage units for the entomology-collection.
          since the latter are more complete, the former are ignored in the calculations.
      */
      $this->calculator->addStaticNumbers( [
        'category' =>  $staticNumber['category'],
        'storageNumber' => ( isset($staticNumber['storageNumber']) ? $staticNumber['storageNumber'] : 0 ),
        'storageCount' => ( isset($staticNumber['storageNumber']) ? ($staticNumber['storageNumber'] * $staticNumber['average']) : 0 ),
        'specimenNumber' => ( isset($staticNumber['specimenNumber']) ? $staticNumber['specimenNumber'] : 0 ),
        'specimenCount' => ( isset($staticNumber['specimenNumber']) ? ($staticNumber['specimenNumber'] * $staticNumber['average']) : 0 ),
      ] );
    }

    $this->calculator->setStorageMountPerCollectionPerDutchProvince( $this->data->storage_netherlandsCollectionMount );
    $this->calculator->setSpecimenKindOfUnitPerCollectionPerDutchProvince( $this->data->specimen_netherlandsCollectionKindOfUnit );
    $this->calculator->setSpecimenPreparationTypePerCollectionPerDutchProvince( $this->data->specimen_netherlandsCollectionPreparationType );
    $this->calculator->setSpecimenCountPerCountryWorld( $this->data->specimen_countPerCountryWorld );
  }

  function calculateResults()
  {
    $this->calculator->runCalculations();
    $this->calculator->roundEstimates();
    $this->calculator->sortCategoryBuckets();
  }

  function getResults()
  {
    $results = [];

    // Row 1: the main counts
    $results['collectionUnitEstimates'] = $this->calculator->getCollectionUnitEstimates();

    $grandUnitsTotal=$this->calculator->getGrandUnitsTotal();
    $results['specimen_total_count'] =  $grandUnitsTotal ;

    $results['specimen_number_of_records'] = $this->data->specimen_totalCount;
    $getAddedStaticNumbers = $this->calculator->getAddedStaticNumbers();
    $results['specimen_bewaareenheden'] = $this->data->storage_catNumberCardinality + $getAddedStaticNumbers['storageNumber'];
    $results['taxon_total_count'] = $this->data->taxon_totalCount;
    $results['multimedia_total_count'] = $this->data->multimedia_totalCount;

    // Second row data:
    // Collectiecategorieën en totaal aantal specimens
    $results['collection_categories'] = [];
    $categoryBuckets=$this->calculator->getCategoryBuckets();
    foreach((array)$categoryBuckets as $key=>$bucket){
      $results['collection_categories'][$bucket['label']] = $bucket['totalUnit_sum'];
    }

    // Third row:
    // Taxonaantallen per rank,
    // Unieke wetenschappelijke namen met specimens, Namen
    $results['taxon_per_rank'] = [];
    foreach((array)$this->data->taxon_groupByRank as $rank => $doc_count){
      $results['taxon_per_rank'][$rank] =  $doc_count ;
    }

    $results['unique_scientific_names_with_specimens'] =  $this->data->specimen_acceptedNamesCardinality ;
    $results['taxonomic_accepted_names'] =  $this->data->taxon_acceptedNamesCardinality ;
    $results['unique_synonyms'] =  $this->data->taxon_synonymCardinality ;
    $results['unique_common_names'] =  $this->data->taxon_vernacularNamesCardinality ;

    // Fourth row:
    // specimens per province; chart and table
    $this->calculator->calculateDutchProvinceNumbers();
    $this->calculator->aggregateDutchProvinceNumbers();
    $provinces = $this->calculator->getDutchProvinceNumbers();
    $results['specimens_per_province'] = [];
    foreach((array)$provinces as $province=>$numbers){
      $results['specimens_per_province'][$province] = [
        "count" =>  $numbers['doc_count'] ,
        "percentage" => $numbers['percentage']
      ];
    }

    // Fifth row:
    // specimens per country
    $this->calculator->setSpecimenCountPerCountryWorld( $this->data->specimen_countPerCountryWorld );
    $this->calculator->calculateWorldNumbers();
    $this->calculator->sortWorldNumbers( 'doc_count', 'desc' );
    $world = $this->calculator->getWorldNumbers();
    $results['specimens_per_country'] = [];
    foreach((array)$world as $country){
      if ($country['label']=='nederland') continue;
      $results['specimens_per_country'][ucwords($country['label'])] =  $country['doc_count'] ;
    }

    // Sixth row:
    // typestatus records per collectie
    $results['typestatus_records_per_collection'] = $this->data->specimen_typeStatusPerCollectionType;

    // Seventh row:
    // meest verzamelde ondersoorten, top verzamelaars
    $results['top_collected_subspecies'] = [];
    foreach((array)$this->data->specimen_perScientificName as $name=>$doc_count){
      $results['top_collected_subspecies'][$name] =  $doc_count ;
    }
    $results['top_collectors'] = $this->data->specimen_collectionTypeCountPerGatherer;


    return $results;
  }

}
