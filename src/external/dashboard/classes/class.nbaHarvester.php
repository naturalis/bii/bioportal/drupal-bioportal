<?php

class nbaHarvester
{
  private nbaInterface $nbaInterface;
  private storageUnits $storageUnits;
  private dataCache $cache;
  private nbaQueries $nbaQueries;
  private array $countData = [];

  /**
   * Prepare the nba harvester
   *
   */
  public function __construct()
  {
    $this->nbaInterface = new nbaInterface;
    $this->nbaInterface->setNbaAddress(config::nbaAddress());

    $this->storageUnits = new storageUnits();
    $this->storageUnits->setDbParams(config::databaseAccessParameters());
    $this->storageUnits->connectDb();

    $this->cache = new dataCache();
    $this->cache->setDbParams(config::databaseAccessParameters());
    $this->cache->setProject('nba_cache');

    $this->nbaQueries = new nbaQueries($this->nbaInterface);
    $this->setupNbaQueries();
  }

  /**
   * Store the harvested data in the cache
   * @return void
   */
  private function storeData(): void
  {
    $this->cache->emptyCache();
    $this->cache->storeData($this->countData);
  }

  /**
   * Setup the queries for the nba
   */
  private function setupNbaQueries(): void
  {
    $this->nbaQueries->setLowerRanks(config::lowerRanks());
    $this->nbaQueries->setIgnorableFullScientificNames(config::ignorableFullScientificNames());
    $this->nbaQueries->setIgnorableCollectors(config::ignorableCollectors());
    $this->nbaQueries->setSourceSystems("specimen", config::sourceSystemsSpecimen());
    $this->nbaQueries->setSourceSystems("taxon", config::sourceSystemsTaxon());
    $this->nbaQueries->setSourceSystems("multimedia", config::sourceSystemsMultimedia());
    $this->nbaQueries->setDutchlands($this->storageUnits->getDutchlands(false));
  }

  /**
   * Get the counts per type
   *
   * @return void
   */
  private function collectionCount(): void
  {
    $collectionOverview = $this->nbaQueries->nbaGetCollectionOverview();
    $this->countData['specimen_prepTypePerCollection'] = $collectionOverview["specimen_prepTypePerCollection"];
    $this->countData['specimen_noPrepTypePerCollection'] = $collectionOverview["specimen_noPrepTypePerCollection"];
    $this->countData['specimen_kindOfUnitPerCollection'] = $collectionOverview["specimen_kindOfUnitPerCollection"];
  }

  /**
   * Get the number of taxons
   *
   * @return void
   */
  private function taxonCount(): void
  {
    $taxonOverview = $this->nbaQueries->nbaGetTaxonOverview();

    $this->countData['taxon_groupByRank'] = $taxonOverview["taxon_groupByRank"];
    $this->countData['taxon_acceptedNamesCardinality'] = $taxonOverview["taxon_acceptedNamesCardinality"];
    $this->countData['taxon_synonymCardinality'] = $taxonOverview["taxon_synonymCardinality"];
    $this->countData['taxon_vernacularNamesCardinality'] = $taxonOverview["taxon_vernacularNamesCardinality"];
  }

  /**
   * Count up the main numbers
   *
   * @return void
   */
  private function mainNumberCount(): void
  {
    $mainNumbers = $this->nbaQueries->nbaGetMainNumbers();
    $this->countData['specimen_totalCount'] = $mainNumbers["specimen_totalCount"];
    $this->countData['taxon_totalCount'] = $mainNumbers["taxon_totalCount"];
    $this->countData['multimedia_totalCount'] = $mainNumbers["multimedia_totalCount"];
  }

  /**
   * Count the number of specimens
   * @return void
   */
  private function specimenCount(): void
  {
    $specimenOverview = $this->nbaQueries->nbaGetSpecimenOverview();
    $this->countData['specimen_acceptedNamesCardinality'] = $specimenOverview["specimen_acceptedNamesCardinality"];
    $this->countData['specimen_typeStatusPerCollectionType'] = $specimenOverview["specimen_typeStatusPerCollectionType"];
    $this->countData['specimen_perScientificName'] = $specimenOverview["specimen_perScientificName"];
    $this->countData['specimen_collectionTypeCountPerGatherer'] = $this->nbaQueries->nbaGetCollectors();
  }

  /**
   * Count the number of documents per collection per mount
   * @return void
   */
  private function individualCount(): void
  {
    $this->countData['storage_sumPerColl_withIndivCount'] = $this->storageUnits->doQuery($this->storageUnits->getQuery("sumPerColl_withIndivCount"));
    $this->countData['storage_docCountPerColl_withoutIndivCount'] = $this->storageUnits->doQuery($this->storageUnits->getQuery("docCountPerColl_withoutIndivCount"));

    foreach ($this->countData['storage_docCountPerColl_withoutIndivCount'] as $key => $val) {
      $this->countData['storage_docCountPerColl_withoutIndivCount'][$key]["mounts"] = $this->storageUnits->doQuery(sprintf($this->storageUnits->getQuery("docCountPerColl_withoutIndivCount_mounts"), $val["INST_COLL_SUBCOLL"]));
    }
  }

  /**
   * Count the number of documents per collection per province
   * @return void
   */
  private function regionalCount(): void
  {
    $this->countData['storage_netherlandsCollectionMount'] = $this->storageUnits->doQuery($this->storageUnits->getQuery("docCountPerNetherlandsProvinces"));

    foreach ($this->countData['storage_netherlandsCollectionMount'] as $provinceKey => $provinceValue) {
      $this->countData['storage_netherlandsCollectionMount'][$provinceKey]["collections"] =
        $this->storageUnits->doQuery(
          sprintf(
            $this->storageUnits->getQuery("docCountCollectionsPerProvince"),
            $provinceValue["stateProvince"]
          )
        );

      foreach ($this->countData['storage_netherlandsCollectionMount'][$provinceKey]["collections"] as $collectionKey => $collectionValue) {
        $this->countData['storage_netherlandsCollectionMount'][$provinceKey]["collections"][$collectionKey]["mounts"] =
          $this->storageUnits->doQuery(
            sprintf(
              $this->storageUnits->getQuery("docCountMountsPerCollectionPerProvince"),
              $provinceValue["stateProvince"],
              $collectionValue["INST_COLL_SUBCOLL"]
            )
          );
      }
    }

    $cardinalityCount = $this->storageUnits->doQuery($this->storageUnits->getQuery("catNumberCardinality"));
    $this->countData['storage_catNumberCardinality'] = $cardinalityCount[0]["doc_count"];

    // calculating provinces
    $this->countData['specimen_netherlandsCollectionKindOfUnit'] = $this->nbaQueries->nbaGetNetherlandsCollectionKindOfUnit();
    $this->countData['specimen_netherlandsCollectionPreparationType'] = $this->nbaQueries->nbaGetNetherlandsCollectionPreparationType();
    $this->countData['specimen_countPerCountryWorld'] = $this->nbaQueries->nbaGetSpecimenCountPerCountryWorld();
  }

  /**
   * Doing the actual steps to harvest the data and store it in the database cache
   */
  public function harvest(): void
  {
    $this->specimenCount();
    $this->mainNumberCount();
    $this->taxonCount();
    $this->collectionCount();
    $this->individualCount();
    $this->regionalCount();
    $this->storeData();
  }
}
