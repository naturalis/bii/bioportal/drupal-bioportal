<?php
    define('BIOPORTAL_ROOT_URL', getenv('BIOPORTAL_ROOT_URL') ? getenv('BIOPORTAL_ROOT_URL') : 'https://bioportal.naturalis.nl');

    define('DB_ACCESS_HOST', getenv('MYSQL_HOST') ? getenv('MYSQL_HOST') : 'database');
    define('DB_ACCESS_USER', getenv('MYSQL_USER') ? getenv('MYSQL_USER') : 'develop');
    define('DB_ACCESS_PASSWORD', getenv('MYSQL_PASSWORD') ? getenv('MYSQL_PASSWORD') : 'develop');
    define('DB_ACCESS_DATABASE', getenv('MYSQL_DATABASE') ? getenv('MYSQL_DATABASE') : 'develop');

    define('NBA_ADDRESS', getenv('NBA_ADDRESS') ? getenv('NBA_ADDRESS') : 'https://api.biodiversitydata.nl/v2/');

    define('LOWER_RANKS',[ "species", "subspecies", "subsp", "ssp." ]); // [ "species", "subspecies", "var.", "subsp", "forma", "cv.", "f.", "subvar."]
    define('SCI_NAMES_TO_IGNORE',[ "Gen. indet. sp. indet.","GEN.INDET. SP.INDET.", "Gen.indet. SP.INDET.", "Gen. indet. sp." ]);
    define('COLLECTORS_TO_IGNORE',[ "Unknown", "Unreadable", "Stud bio" ]);

    define('SOURCE_SYSTEMS_SPECIMEN',["BRAHMS","CRS"]);
    define('SOURCE_SYSTEMS_TAXON',["COL","NSR","DCSR"]);
    define('SOURCE_SYSTEMS_MULTIMEDIA',["BRAHMS","CRS","NSR","DCSR"]);
