<?php
include_once("classes/class.nbaInterface.php");
include_once("classes/class.storageUnits.php");
include_once("classes/class.nbaQueries.php");
include_once("classes/class.dataCache.php");
include_once("classes/class.collectionUnitCalculation.php");
include_once("config/class.config.php");

include_once("classes/class.nbaHarvester.php");


set_time_limit( 3600 );

$start = microtime(true);
echo "\nStarting harvesting at " . date('l jS \of F Y h:i:s A');
$harvester = new nbaHarvester();
$harvester->harvest();
echo "\nFinished harvesting in " . round(microtime(true) - $start,1) . " seconds\n";
