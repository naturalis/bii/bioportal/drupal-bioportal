<?php
include_once("classes/class.nbaInterface.php");
include_once("classes/class.storageUnits.php");
include_once("classes/class.nbaQueries.php");
include_once("classes/class.contentBlocks.php");
include_once("classes/class.translator.php");
include_once("classes/class.dashboardCollector.php");

include_once("config/class.config.php");

$dashboardCollector = new dashboardCollector();
$dashboardCollector->calculateResults();
$results = $dashboardCollector->getResults();

header('Content-Type: application/json');
echo json_encode($results);
