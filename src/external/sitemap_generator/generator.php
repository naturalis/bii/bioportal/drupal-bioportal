<?php
require_once 'BioportalSitemap.php';

$sitemap = new \BioportalSitemap();
$sitemap->setBioportalUrl(
    rtrim(getenv('BASE_URL'), "/")
);
$sitemap->run();

