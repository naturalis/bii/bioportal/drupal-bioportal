<?php

/**
 * @file
 * Drupal site-specific configuration file.
 */

/**
 * Database settings.
 */
$databases['default']['default'] = [
   'database' => getenv('MYSQL_DATABASE'),
   'username' => getenv('MYSQL_USER'),
   'password' => getenv('MYSQL_PASSWORD'),
   'host' => 'database',
   'port' => '3306',
   'driver' => 'mysql',
   'prefix' => '',
   'charset' => 'utf8mb4',
   'collation' => 'utf8mb4_general_ci',
   'init_commands' => [
    'isolation_level' => 'SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED',
   ]
 ];

$databases['migrate']['default'] = [
  'database' => 'drupal7',
  'username' => 'root',
  'password' => 'root',
  'host' => 'database',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
];

$settings['container_yamls'][] = __DIR__ . '/../config/services.yml';

/**
 * Monolog settings.
 */
$settings['container_yamls'][] = __DIR__ . '/../config/monolog.services.yml';

/**
 * Salt for one-time login links, cancel links, form tokens, etc.
 */
$settings['hash_salt'] = getenv('DRUPAL_SALT');

/**
 * Location of the site configuration files.
 */
$settings['config_sync_directory'] = DRUPAL_ROOT . '/../config/sync';

/**
 * Access control for update.php script.
 */
$settings['update_free_access'] = FALSE;

/**
 * Public file path.
 */
$settings['file_public_path'] = 'sites/default/files';

/**
 * Disable authorized file system operations.
 */
$settings['allow_authorize_operations'] = FALSE;

/**
 * Trusted host configuration.
 */
$settings['trusted_host_patterns'] = [
    '^drupal-webserver$',
    '^bioportal\.dryrun\.link$',
    '.*\.hosts\.naturalis\.io$',
    '^bioportal\.naturalis\.nl$',
];

/**
 * The default list of directories that will be ignored by Drupal's file API.
 */
$settings['file_scan_ignore_directories'] = [
  'node_modules',
];

/**
 * The default number of entities to update in a batch process.
 */
$settings['entity_update_batch_size'] = 50;

/**
 * Entity update backup.
 */
$settings['entity_update_backup'] = FALSE;

/**
 * Node migration type.
 */
$settings['migrate_node_migrate_type_classic'] = FALSE;

/**
 * Load local development override configuration, if available.
 */
if (file_exists(__DIR__ . '/../settings.local.php')) {
  include __DIR__ . '/../settings.local.php';
}

